<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;

use App\Http\Controllers\HistoriqueCallController;

Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login');
    Route::post('register', 'register');
    Route::post('logout', 'logout');
    Route::put('update_profile', 'update_profile')->middleware('auth:sanctum');
    Route::get('liste_user', 'getListeUser')->middleware('auth:sanctum');

});

Route::controller(HistoriqueCallController::class)->group(function () {
    Route::post('historique', 'store')->middleware('auth:sanctum');
    Route::get('gethistorique', 'getHistorique')->middleware('auth:sanctum');
});

