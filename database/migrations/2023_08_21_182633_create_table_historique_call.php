<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('historique_call', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('emetteur_id');
            $table->unsignedBigInteger('receptteur_id');

            $table->foreign('emetteur_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('receptteur_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');


            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('table_historique_call');
    }
};
