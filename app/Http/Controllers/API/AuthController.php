<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum', ['except' => ['login', 'register']]);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            return response()->json([
                'user' => $user,
                'authorization' => [
                    'token' => $user->createToken('ApiToken')->plainTextToken,
                    'type' => 'bearer',
                ]
            ]);
        }

        return response()->json([
            'message' => 'Invalid credentials',
        ], 401);
    }

    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

         
        if ($request->has('filleul') && !empty($request->get('filleul'))) {
            $list_user = $request->get('filleul');
            foreach ($list_user as $user_parrain) {
                $new_user = User::create([
                    'name' => $user_parrain['name'],
                    'email' => $user_parrain['email'],
                    'password' => Hash::make($user_parrain['password']),
                    "parrain_id" => $user->id
                ]);
            }
        }

        return response()->json([
            'message' => 'User created successfully',
        ]);
    }

    public function logout()
    {
        Auth::user()->tokens()->delete();
        return response()->json([
            'message' => 'Successfully logged out',
        ]);
    }

    public function refresh()
    {
        return response()->json([
            'user' => Auth::user(),
            'authorisation' => [
                'token' => Auth::refresh(),
                'type' => 'bearer',
            ]
        ]);
    }


    public function update_profile(Request $request){
        $user = Auth::user();
        if($request->has('name')){
            $user->name = $request->get('name');
        }
        if($request->has('email')){
            $user->email = $request->get('email');
        }
        if($request->has('password')){
            $user->password = Hash::make($request->get('password'));
        }
      
        
        
        $user->save();

        return response()->json([
            'message' => 'User update successfully',
        ]);
    }


    public function getListeUser(){
        $liste_user=User::all();
        return response()->json([
            'message' => 'User update successfully',
            'data'=> $liste_user
        ]);
    }
}
