<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HistoriqueCall;
use Illuminate\Support\Facades\Auth;

class HistoriqueCallController extends Controller
{
    //

    public function store(Request $request){

       
        $request->validate([
            'date_debut' => 'required',
            'date_fin' => 'required',
            'receptteur_id' => 'required',
            'emetteur_id'=>'required'
        ]);
      
        $HistoriqueCall = HistoriqueCall::create([
            'date_debut' => $request->date_debut,
            'date_fin' => $request->date_fin,
            'receptteur_id' => $request->receptteur_id,
            'emetteur_id' => $request->emetteur_id,
        ]);
      

     

        return response()->json([
            'message' => 'historique created successfully',
            'data'=> $HistoriqueCall
        ]);
    }

    public function getHistorique(){
        $user = Auth::user();

        $liste_historique=HistoriqueCall::where('receptteur_id',$user->id)
        ->orwhere('emetteur_id',$user->id)->get();
        
        foreach($liste_historique as $historique){
            $historique->user_recept;
            $historique->user_emmetteur;
        }
        return response()->json([
            'message' => 'User created successfully',
            'data'=> $liste_historique
        ]);
    }
}
