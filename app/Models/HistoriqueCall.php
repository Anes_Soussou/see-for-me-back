<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoriqueCall extends Model
{
    use HasFactory;

    protected $table = 'historique_call';
    protected $fillable = [
        'date_debut',
        'date_debut',
        'emetteur_id',
        'receptteur_id'
    ];

    public function user_recept()
    {
        return $this->belongsTo(User::class, 'receptteur_id');
    }

    public function user_emmetteur()
    {
        return $this->belongsTo(User::class, 'emetteur_id');
    }

}
